/**
 * Created by chadovich_cr on 3/14/16.
 */
var env = process.env.NODE_ENV || 'dev';

var config = {
    server: {
        port: parseInt(process.env.PORT) || 3102,
        host: process.env.HOST || 'localhost'
    },
    db: {
        host: process.env.DB_HOST || 'localhost',
        port: parseInt(process.env.DB_PORT) || 3306,
        dbname: process.env.DB_NAME || 'travels_social',
        user: process.env.DB_USER || 'root',
        password: process.env.DB_PASSWORD || 'root',
        charset: 'utf8mb4',
        connectionRetryCount: 5,
        delayBeforeReconnect: 3000,
        showErrors: true
    },
    uploads: {
        product: {
            uploadBase: 'frontend',
            uploadDir: './frontend/uploads',
            maxImageSize: 4000
        }
    }
};

if (env === 'test') {
    config.db.showErrors = false;
}

module.exports = config;
