/**
 * Created by chadovich_cr on 3/14/16.
 */
var fs = require('fs');

module.exports = {
    index: function (req, res, next) {
        fs.createReadStream(__dirname + "/../../frontend/index.html")
            .pipe(res);
    }
};
