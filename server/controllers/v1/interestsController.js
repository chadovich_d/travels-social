/**
 * Created by chadovich_cr on 3/24/16.
 */
var interests = require('../../models/v1').interests;
var logger = require('../../utils/logger');

/**
 * load interests list
 * @param req
 * @param res
 * @param next
 * @returns {*}
 * @private
 */
function _loadAllInterests(req, res, next) {
    var limit = parseInt(req.query.limit) || 0;

    if(isNaN(limit)) {
        var err = new Error();
        err.status = 400;
        err.message = 'Incorrect limit param!';
        return next(err);
    }

    var params = {
        limit: limit
    };

    interests.getInterestsList(params, function(err, result) {
        if(err) {
            logger.error('Load interests from DB error: ', err);
            return next(err);
        }

        res.status(200);
        res.send({
            'status': 'success',
            'interests': result
        });
    });
}

module.exports = {
    loadAllInterests: [_loadAllInterests]
};