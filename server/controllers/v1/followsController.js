/**
 * Created by chadovich_cr on 3/31/16.
 */
var follows = require('../../models/v1').follows;
var users = require('../../models/v1').users;
var logger = require('../../utils/logger');

/**
 * check token
 * @param req
 * @param res
 * @param next
 * @returns {*}
 * @private
 */
function _checkToken(req, res, next) {
    var headersAuthorization = req.headers.authorization;
    var index = headersAuthorization.indexOf(' ');
    var fromUserToken = headersAuthorization.substring(index + 1);

    if(!fromUserToken || fromUserToken === 'undefined') {
        var err = new Error();
        err.status = 401;
        err.message = 'You must be authorized!';
        return next(err);
    }

    var params = {
        token: fromUserToken
    };

    users.checkToken(params, function(err, result) {
        if(err) {
            logger.error('Check token error: ', err);
            return next(err);
        }

        if(!result || !result.length) {
            var error = new Error();
            error.status = 401;
            error.message = 'You must be authorized!';
            error.details = 'Check token error!';
            return next(error);
        }

        req.fromUserId = result[0].userId;
        next();
    });
}

/**
 * add follow
 * @param req
 * @param res
 * @param next
 * @returns {*}
 * @private
 */
function _addFollow(req, res, next) {
    var fromUserId = parseInt(req.fromUserId);
    var toUserId = parseInt(req.body.toUserId);

    if(isNaN(fromUserId || toUserId)) {
        var err = new Error();
        err.status = 400;
        err.message = 'Incorrect users params!';
        return next(err);
    }

    var params = {
        fromUserId: fromUserId,
        toUserId: toUserId
    };

    follows.addFollow(params, function(err, result) {
        if(err) {
            logger.error('Add follow error: ', err);
            return next(err);
        }

        res.status(201);
        res.send();
    });
}

/**
 * remove follow
 * @param req
 * @param res
 * @param next
 * @returns {*}
 * @private
 */
function _deleteFollow(req, res, next) {
    var fromUserId = parseInt(req.fromUserId);
    var toUserId = parseInt(req.query.toUserId);

    if(isNaN(fromUserId || toUserId)) {
        var err = new Error();
        err.status = 400;
        err.message = 'Incorrect users params!';
        return next(err);
    }

    var params = {
        fromUserId: fromUserId,
        toUserId: toUserId
    };

    follows.deleteFollow(params, function(err, result) {
        if(err) {
            logger.error('Delete follow error: ', err);
            return next(err);
        }

        res.status(201);
        res.send();
    });
}

module.exports = {
    addFollow: [_checkToken, _addFollow],
    deleteFollow: [_checkToken, _deleteFollow]
};