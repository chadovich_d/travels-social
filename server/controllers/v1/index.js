module.exports = {
    users       : require('./usersController'),
    interests   : require('./interestsController'),
    likes       : require('./likesController'),
    follows     : require('./followsController')
};