var users = require('../../models/v1').users;
var crypto = require('crypto');
var validator = require('../../utils/bodyValidator');
var logger = require('../../utils/logger');

/**
 * create user request validation
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function validateCreateUserRequest(req, res, next) {
    return validator({
        properties: {
            email: {
                required: true,
                allowEmpty: false,
                type: 'string',
                format: 'email'
            },
            password: {
                required: true,
                allowEmpty: false,
                type: 'string',
                minLength: 6
            },
            firstName: {
                required: true,
                allowEmpty: false,
                type: 'string',
                minLength: 2,
                maxLength: 20
            },
            lastName: {
                required: true,
                allowEmpty: false,
                type: 'string',
                minLength: 4,
                maxLength: 30
            },
            fromCountry: {
                required: true,
                allowEmpty: false,
                type: 'string',
                minLength: 2,
                maxLength: 30
            },
            fromCity: {
                required: true,
                allowEmpty: false,
                type: 'string',
                minLength: 2,
                maxLength: 30
            }
        }
    })(req, res, next);
}

/**
 * generate salt
 * @param req
 * @param res
 * @param next
 * @private
 */
function _generateSalt(req, res, next) {
    req.salt = crypto.randomBytes(16).toString('hex');
    next();
}

/**
 * check email unique during registration
 * @param req
 * @param res
 * @param next
 * @private
 */
function _checkEmailUnique(req, res, next) {
    var params = {
        email: req.body.email
    };

    users.checkEmailUnique(params, function(err, result) {
        if(err) {
            return next(err);
        }
        if(result && result.length) {
            var error = new Error();
            error.status = 500;
            error.message = 'E-mail already exists';
            return next(error);
        }
        next();
    });
}

/**
 * generate password hash
 * @param req
 * @param res
 * @param next
 * @private
 */
function _generatePasswordHash(req, res, next) {
    var password = req.body.password;
    var hash = crypto.createHash('sha256');
    hash.update(password + req.salt);

    req.secretPassword = hash.digest('hex');
    next();
}

/**
 * create user request
 * @param req
 * @param res
 * @param next
 * @private
 */
function _createUser(req, res, next) {
    var params = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.secretPassword,
        salt: req.salt,
        fromCountry: req.body.fromCountry,
        fromCity: req.body.fromCity,
        currentPlaceLng: req.body.currentPlaceLng,
        currentPlaceLat: req.body.currentPlaceLat,
        currentPlace: req.body.currentPlace,
        aboutText: req.body.aboutText || 'No data'
    };

    users.addUser(params, function (err, result) {
        if(err) {
            return next(err);
        }

        req.userId = result.insertId;
        next();
    });
}

/**
 * set user's interests
 * @param req
 * @param res
 * @param next
 * @private
 */
function _setUserInterests(req, res, next) {
    var params = [];
    var userInterests = req.body.userInterests;
    if(!userInterests.length) {
        res.status(201);
        res.send({
            'status': 'success',
            'message': 'user created without interests'
        })
    }

    for(var i = 0, length = userInterests.length; i < length; i ++) {

        var subArray = [];
        subArray.push(req.userId);
        subArray.push(userInterests[i]);

        params.push(subArray);

    }

    users.setInterests(params, function(err, result) {
        if(err) {
            return next(err);
        }

        res.status(201);
        res.send({
            'status': 'success',
            'message': 'user created'
        })
    });
}

/**
 * log in user request validation
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function validateLoginUserRequest(req, res, next) {
    return validator({
        properties: {
            email: {
                required: true,
                allowEmpty: false,
                type: 'string',
                format: 'email'
            },
            password: {
                required: true,
                allowEmpty: false,
                type: 'string',
                minLength: 6
            }
        }
    })(req, res, next);
}

/**
 * check log in email
 * @param req
 * @param res
 * @param next
 * @private
 */
function _checkLoginEmail(req, res, next) {
    var params = {
        email: req.body.email
    };

    users.checkLoginEmail(params, function(err, result) {
        if(err) {
            return next(err);
        }
        if(!result || result.length === 0) {
            var error = new Error();
            error.message = 'E-mail does not exist!';
            error.status = 404;
            return next(error);
        }

        req.salt = result[0].salt;
        req.firstName = result[0].firstName;
        req.lastName = result[0].lastName;

        next();
    });
}

/**
 * generate password hash during log in
 * @param req
 * @param res
 * @param next
 * @private
 */
function _generateLoginPasswordHash(req, res, next) {
    var salt = req.salt;
    var password = req.body.password;
    var hash = crypto.createHash('sha256');
    hash.update(password + salt);

    req.secretPassword = hash.digest('hex');
    next();
}

/**
 * check log in password
 * @param req
 * @param res
 * @param next
 * @private
 */
function _checkLoginPassword(req, res, next) {
    var params = {
        password: req.secretPassword
    };

    users.checkLoginPassword(params, function(err, result) {
        if(err) {
            return next(err);
        }
        if(!result || result.length === 0) {
            var error = new Error();
            error.message = 'Wrong password!';
            error.status = 403;
            return next(error);
        }
        req.userId = result[0].id;
        next();
    });
}

/**
 * generate token
 * @param req
 * @param res
 * @param next
 * @private
 */
function _generateToken(req, res, next) {
    var token = crypto.randomBytes(32).toString('hex');

    var params = {
        token: token,
        userId: req.userId
    };

    users.saveTokenToDB(params, function(err, result) {
        if(err) {
            return next(err);
        }
        res.status(200);
        res.send({
            'status': 'success',
            'token': token,
            'user': req.userId,
            'firstName': req.firstName,
            'lastName': req.lastName
        })
    });
}

/**
 * log out user request validation
 * @param req
 * @param res
 * @param next
 * @returns {*}
 */
function validateLogOutRequest(req, res, next) {
    return validator({
        properties: {
            id: {
                required: true,
                allowEmpty: false,
                type: 'string'
            }
        }
    })(req, res, next);
}

/**
 * delete token after user log out
 * @param req
 * @param res
 * @param next
 * @returns {*}
 * @private
 */
function _deleteToken(req, res, next) {
    var id = parseInt(req.body.id);

    if(isNaN(id)) {
        var err = new Error();
        err.status = 400;
        err.message = 'Incorrect id param!';
        return next(err);
    }

    var params = {
        id: id
    };

    users.deleteToken(params, function(err, result) {
        if(err) {
            return next(err);
        }

        res.status(204);
        res.send();
    });
}

/**
 * check token
 * @param req
 * @param res
 * @param next
 * @private
 */
function _checkToken(req, res, next) {
    var headersAuthorization = req.headers.authorization;
    var index = headersAuthorization.indexOf(' ');
    var token = headersAuthorization.substring(index + 1);

    var params = {
        token: token
    };

    users.checkToken(params, function(err, result) {
        if(err) {
            return next(err);
        }
        if(!result || !result.length) {
            req.userId = null;
            return next(req.userId);
        }

        req.userId = result[0].userId;
        next();
    });
}

/**
 * get user profile
 * @param req
 * @param res
 * @param next
 * @returns {*}
 * @private
 */
function _getUserProfile(req, res, next) {
    req.id = parseInt(req.params.id);

    if(isNaN(req.id)) {
        var error = new Error();
        error.status = 400;
        error.message = 'Incorrect id param!';
        return next(error);
    }

    var params = {
        userId: req.userId,
        id: req.id
    };

    users.getUserProfile(params, function(err, result) {
        if(err) {
            return next(err);
        }
        if(!result || !result.length) {
            var error = new Error();
            error.status = 404;
            error.message = 'User not found!';
            error.details = 'Incorrect user id!';
            return next(error);
        }

        res.status(200);
        res.send({
            'status': 'success',
            'user': result[0]
        })
    });
}

/**
 * get list of user's interests
 * @param req
 * @param res
 * @param next
 * @returns {*}
 * @private
 */
function _getUserInterests(req, res, next) {
    if(isNaN(req.id)) {
        var error = new Error();
        error.status = 400;
        error.message = 'Incorrect id param!';
        return next(error);
    }

    var params = {
        id: req.id
    };

    users.getUserInterests(params, function(err, result) {
        if(err) {
            return next(err);
        }

        if(!result || !result.length) {
            var error = new Error();
            error.status = 500;
            error.message = 'Internal server error!';
            return next(error);
        }

        res.status(200);
        res.send({
            'status': 'success',
            'user': req.user,
            'interests': result
        })
    });
}

/**
 * count users amount in DB
 * @param req
 * @param res
 * @param next
 * @private
 */
function _countUsersAmount(req, res, next) {

    users.countUsersAmount([], function(err, result) {
        if(err) {
            return next(err);
        }

        if(!result || !result.length) {
            var error = new Error();
            error.status = 409;
            error.message = 'No users are found!';
            return next(error);
        }

        req.usersAmount = result[0].usersAmount;
        next();
    });
}

/**
 * get list of users
 * @param req
 * @param res
 * @param next
 * @private
 */
function _loadUsersList(req, res, next) {
    var limit = parseInt(req.query.limit);
    var offset = parseInt(req.query.offset);
    var userId = req.query.userId;

    var params = {
        limit: limit >= 0 ? limit : null,
        offset: offset >= 0 ? offset : null,
        userId: userId ? userId : null
    };

    users.loadUsersList(params, function(err, result) {
        if(err) {
            return next(err);
        }

        if(!result || !result.length) {
            var error = new Error();
            error.status = 500;
            error.message = 'Internal server error!';
            return next(error);
        }

        res.status(200);
        res.send({
            'status': 'success',
            'users': result,
            'usersAmount': req.usersAmount
        })
    });
}

module.exports = {
    createUser: [validateCreateUserRequest, _generateSalt, _checkEmailUnique, _generatePasswordHash, _createUser, _setUserInterests],
    loginUser: [validateLoginUserRequest, _checkLoginEmail, _generateLoginPasswordHash, _checkLoginPassword, _generateToken],
    logOut: [validateLogOutRequest, _deleteToken],
    getUserProfile: [_checkToken, _getUserProfile],
    loadUsersList: [_countUsersAmount, _loadUsersList]
};