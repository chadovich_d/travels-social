/**
 * Created by chadovich_cr on 3/24/16.
 */
var activeRecord = require('../../utils/active_record');
var AR = new activeRecord();

function Users () {}

Users.prototype.addUser = function (params, done) {
    var queryParams = [
        params.firstName,
        params.lastName,
        params.email,
        params.password,
        params.salt,
        params.fromCountry,
        params.fromCity,
        params.currentPlaceLng,
        params.currentPlaceLat,
        params.currentPlace,
        params.aboutText
    ];

    var query = [
        'INSERT INTO users',
        '(firstName, lastName, email, password, salt, ',
        'fromCountry, fromCity, currentPlaceLng, ',
        'currentPlaceLat, currentPlace, aboutText)',
        'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
    ].join('\n');

    AR.execute(query, queryParams, done);
};

Users.prototype.checkEmailUnique = function(params, done) {
    var queryParams = [
        params.email
    ];

    var query = 'SELECT * FROM `users` WHERE `email` = ?';

    AR.execute(query, queryParams, done);
};

Users.prototype.setInterests = function(params, done) {
    var query = 'INSERT INTO `users_to_interests` (`userId`, `interestId`) VALUES ?';

    AR.execute(query, [params], done);
};

Users.prototype.checkLoginEmail = function(params, done) {
    var queryParams = [
        params.email
    ];

    var query = 'SELECT * FROM `users` WHERE `email` = ?';

    AR.execute(query, queryParams, done);
};

Users.prototype.checkLoginPassword = function(params, done) {
    var queryParams = [
        params.password
    ];

    var query = 'SELECT * FROM `users` WHERE `password` = ?';

    AR.execute(query, queryParams, done);
};

Users.prototype.checkToken = function (params, done) {
    var queryParams = [
        params.token
    ];

    var query = 'SELECT userId FROM `tokens` WHERE `token` = ?';

    AR.execute(query, queryParams, done);
};

Users.prototype.saveTokenToDB = function (params, done) {
    var queryParams = [
        params.userId,
        params.token
    ];

    var query = 'INSERT INTO `tokens` (`userId`, `token`) VALUES (?, ?)';

    AR.execute(query, queryParams, done);
};

Users.prototype.getUserProfile = function(params, done) {
    var queryParams = [
        params.userId,
        params.userId,
        params.id
    ];

    var query = [
        'SELECT u.id, u.firstName, u.lastName, u.fromCountry, u.fromCity,',
        'u.photo, u.avatar, u.postsRating, u.postsCount,',
        'u.commentsCount, u.profileLikesCount, u.visitedCountriesCount,',
        'u.currentPlaceLng, u.currentPlaceLat, u.currentPlace, u.aboutText,',
        'GROUP_CONCAT(i.name) as userInterests, l.id as liked, f.id as followed,',
        'u.profileFollowersCount',
        'FROM users u',
        'LEFT JOIN users_to_interests ui ON ui.userId = u.id',
        'LEFT JOIN interests i ON ui.interestId = i.id',
        'LEFT JOIN likes l ON l.fromUserId = ? AND l.toUserId = u.id',
        'LEFT JOIN follows f ON f.fromUserId = ? AND f.toUserId = u.id',
        'WHERE u.id = ?',
        'GROUP BY u.id'
    ].join('\n');

    AR.execute(query, queryParams, done);
};

Users.prototype.getUserInterests = function(params, done) {
    var queryParams = [
        params.id
    ];

    var query = [
        'SELECT name FROM interests i',
        'INNER JOIN users_to_interests ui ON ui.interestId = i.id',
        'WHERE ui.userId = ?'
    ].join('\n');

    AR.execute(query, queryParams, done);
};

Users.prototype.countUsersAmount = function(params, done) {
    var query = 'SELECT COUNT( * ) AS usersAmount FROM users';

    AR.execute(query, [], done);

};

Users.prototype.loadUsersList = function(params, done) {
    var queryParams = [
        params.userId,
        params.userId
    ];

    var query = [
        'SELECT u.id, u.firstName, u.lastName, u.fromCountry, u.fromCity,',
        'u.photo, u.avatar, u.postsRating, u.postsCount,',
        'u.commentsCount, u.profileLikesCount, u.visitedCountriesCount,',
        'u.currentPlaceLng, u.currentPlaceLat, u.currentPlace, u.aboutText,',
        'l.id as liked, GROUP_CONCAT(i.name) as userInterests,',
        'f.id as followed',
        'FROM users u',
        'LEFT JOIN users_to_interests ui ON ui.userId = u.id',
        'LEFT JOIN interests i ON ui.interestId = i.id',
        'LEFT JOIN likes l ON l.fromUserId = ? AND toUserId = u.id',
        'LEFT JOIN follows f ON f.fromUserId = ? AND f.toUserId = u.id',
        'GROUP BY u.id',
        'ORDER BY u.firstName'
    ].join('\n');

    if(params.limit) {
        queryParams.push(params.limit);
        query += ' LIMIT ' + params.limit;
    }

    if(params.offset) {
        queryParams.push(params.limit);
        query += ' OFFSET ' + params.offset;
    }

    AR.execute(query, queryParams, done);

};

Users.prototype.deleteToken = function(params, done) {
    var queryParams = [
        params.id
    ];

    var query = 'DELETE FROM `tokens` WHERE `userId`= ?';

    AR.execute(query, queryParams, done);
};

module.exports = Users;