/**
 * Created by chadovich_cr on 3/24/16.
 */
var Users = require('./users');
var Interests = require('./interests');
var Likes = require('./likes');
var Follows = require('./follows');

module.exports = {
    users: new Users(),
    interests: new Interests(),
    likes: new Likes(),
    follows: new Follows()
};