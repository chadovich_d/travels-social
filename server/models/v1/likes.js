/**
 * Created by chadovich_cr on 3/30/16.
 */
var activeRecord = require('../../utils/active_record');
var AR = new activeRecord();

function Likes () {}

Likes.prototype.addLike = function(params, done) {
    var queryParams = [
        params.fromUserId,
        params.toUserId
    ];

    var query = [
        'INSERT INTO `likes` ',
        '(`fromUserId`, `toUserId`) ',
        'VALUES (?, ?)'
    ].join('\n');

    AR.execute(query, queryParams, done);
};

Likes.prototype.deleteLike = function(params, done) {
    var queryParams = [
        params.fromUserId,
        params.toUserId
    ];

    var query = 'DELETE FROM `likes` WHERE `fromUserId` = ? AND `toUserId` = ?';

    AR.execute(query, queryParams, done);
};

module.exports = Likes;