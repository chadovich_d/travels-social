/**
 * Created by chadovich_cr on 3/31/16.
 */
var activeRecord = require('../../utils/active_record');
var AR = new activeRecord();

function Follows () {}

Follows.prototype.addFollow = function(params, done) {
    var queryParams = [
        params.fromUserId,
        params.toUserId
    ];

    var query = [
        'INSERT INTO `follows` ',
        '(`fromUserId`, `toUserId`) ',
        'VALUES (?, ?)'
    ].join('\n');

    AR.execute(query, queryParams, done);
};

Follows.prototype.deleteFollow = function(params, done) {
    var queryParams = [
        params.fromUserId,
        params.toUserId
    ];

    var query = 'DELETE FROM `follows` WHERE `fromUserId` = ? AND `toUserId` = ?';

    AR.execute(query, queryParams, done);
};

module.exports = Follows;