/**
 * Created by chadovich_cr on 3/24/16.
 */
var activeRecord = require('../../utils/active_record');
var AR = new activeRecord();

function Interests () {}

Interests.prototype.getInterestsList = function(params, done) {
    var queryParams = [];
    var query = '';

    if(params.limit) {
        queryParams = [params.limit];
        query = 'SELECT * FROM `interests` LIMIT ?';
    } else {
        query = 'SELECT * FROM `interests`';
    }

    AR.execute(query, queryParams, done);
};

module.exports = Interests;