/**
 * Created by chadovich_cr on 3/14/16.
 */
var express = require('express');
var router = express.Router();
var passport = require('passport');

var staticController = require('../controllers/staticController');
var usersControllerV1 = require('../controllers/v1').users;
var interestsControllerV1 = require('../controllers/v1').interests;
var likesControllerV1 = require('../controllers/v1').likes;
var followsControllerV1 = require('../controllers/v1').follows;

//static file
router.get('/index', staticController.index);
router.get('/', staticController.index);

// API

// v1 routes
// users
router.post('/api/v1/users', usersControllerV1.createUser);
router.get('/api/v1/users', usersControllerV1.loadUsersList);
router.post('/api/v1/login', usersControllerV1.loginUser);
router.post('/api/v1/logout', usersControllerV1.logOut);
router.get('/api/v1/profile/:id', usersControllerV1.getUserProfile);

// interests
router.get('/api/v1/interests', interestsControllerV1.loadAllInterests);

// likes
router.post('/api/v1/likes', likesControllerV1.addLike);
router.delete('/api/v1/likes', likesControllerV1.deleteLike);

// follows
router.post('/api/v1/follow', followsControllerV1.addFollow);
router.delete('/api/v1/follow', followsControllerV1.deleteFollow);

// static
router.get('/*', staticController.index);

module.exports = router;
