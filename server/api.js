/**
 * Created by chadovich_cr on 3/14/16.
 */

var express = require('express');
var bodyParser = require('body-parser');
var errorHandler = require('../server/utils/errorHandler');
var router = require('../server/routes');

//initialize the app
var app = module.exports = express();

//set up static files directory
app.use(express.static(__dirname + '/../frontend'));

app.use(bodyParser.json());

app.use(router);

//set up http error handler
app.use(errorHandler(app));