/**
 * Created by chadovich_cr on 3/14/16.
 */
var errorHandler = require('express-error-handler');

module.exports = function (app) {
    return  errorHandler({
        server: app,
        shutdown: function(err){
            console.error('shutdown called:');
        },
        handlers: {
            '500': function err500(err, req, res, next) {
                res.status(500);
                if (err.message) {
                    res.send({
                        error: 500,
                        details: err.message
                    });
                } else {
                    res.send({
                        error: 500,
                        details: 'Something unexpected happened. The problem has been logged and we\'ll look into it!'
                    });
                }
            },
            '503': function err503(err, req, res, next) {
                res.status(503);
                res.send({
                    error: 503,
                    details: 'We\'re experiencing heavy load, please try again later'
                });
            },
            '409': function err409(err, req, res, next) {
                res.status(409);
                if (err.message) {
                    res.send({
                        error: 409,
                        details: err.message
                    });
                } else {
                    res.send({
                        error: 409,
                        details: "The specified resource already exists"
                    });
                }
            },
            '405': function err405(err, req, res, next) {
                res.status(405).send({
                    error: 405,
                    details: "Method not allowed"
                });
            },
            '404': function err404(err, req, res, next) {
                res.status(404);
                if (err.message) {
                    return res.send({
                        error: 404,
                        details: err.message
                    });
                } else {
                    return res.send({
                        error: 404,
                        message: "Not Found"
                    });
                }
            },
            '401': function err401(err, req, res, next) {
                res.status(401);
                if (err.message) {
                    res.send({
                        error: 401,
                        details: err.message
                    });
                } else {
                    res.send({
                        error: 401,
                        details: "Unauthorized"
                    });
                }
            },
            '400': function err400(err, req, res, next) {
                res.status(400);
                if (err.message) {
                    res.send({
                        error: 400,
                        details: err.message
                    });
                } else {
                    res.send({
                        error: 400,
                        details: "Invalid request"
                    });
                }
            }
        }
    })
};