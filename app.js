/**
 * Created by chadovich_cr on 3/14/16.
 */
process.on('uncaughtException', function (err) {
    console.error((new Date).toUTCString() + ' uncaughtException:', err);
    console.error(err.stack);
    process.exit(1);
});

var config = require('./config');
var logger = require('./server/utils/logger');

var app = require('./server/api');

app.listen(config.server.port, function () {
    logger.log('info', "Web server successfully started at port " + config.server.port);
});