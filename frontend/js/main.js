/**
 * Created by chadovich_cr on 3/14/16.
 */
require.config({
    baseUrl: "../js",
    paths: {
        'jquery': '../lib/jquery-1.12.0.min',
        'domReady': '../bower_components/domReady/domReady',

        'angular': '../bower_components/angular/angular.min',
        'angular-route': '../bower_components/angular-route/angular-route.min',
        'angularAMD': '../bower_components/angularAMD/angularAMD.min',
        'ngCookies': '../bower_components/angular-cookies/angular-cookies.min',
        'ngAnimate': '../bower_components/angular-animate/angular-animate.min',
        'ngTouch': '../bower_components/angular-touch/angular-touch.min',

        'jquery-validate': '../lib/jquery.validate',

        'ui.bootstrap': '../bower_components/angular-ui-bootstrap/dist/ui-bootstrap-tpls',

        'utils': 'services/utils',

        'MainCtrl': 'controllers/MainCtrl',
        'BlogCtrl': 'controllers/BlogCtrl',
        'CompaniesCtrl': 'controllers/CompaniesCtrl',
        'HomeCtrl': 'controllers/HomeCtrl',
        'HotCtrl': 'controllers/HotCtrl',
        'MembersCtrl': 'controllers/MembersCtrl',
        'ArticleCtrl': 'controllers/ArticleCtrl',
        'ProfileCtrl': 'controllers/ProfileCtrl'
    },
    shim: {

        'angular': {
            exports: 'angular'
        },

        'jquery-validate': ['jquery'],

        'angular-route': ['angular'],
        'angularAMD': ['angular'],
        'ngCookies': ['angular'],
        'ui.bootstrap': ['angular', 'ngAnimate', 'ngTouch'],
        'ngAnimate': ['angular'],
        'ngTouch': ['angular']

    },

    deps: ['app', 'config', 'MainCtrl']
});