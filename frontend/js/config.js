/**
 * Created by chadovich_cr on 3/14/16.
 */
define([
    'app',
    'angular',
    'angularAMD',
    'angular-route',
    'domReady!'
],
    function (app, angular, angularAMD) {

        app.config(['$routeProvider', '$httpProvider', '$locationProvider',
            function($routeProvider, $httpProvider, $locationProvider) {

                $routeProvider.
                    when('/blog', angularAMD.route({
                        templateUrl: 'pages/blog.html',
                        controller: 'BlogCtrl'
                    })).
                    when('/blog/:id', angularAMD.route({
                        templateUrl: 'pages/article.html',
                        controller: 'ArticleCtrl'
                    })).
                    when('/members', angularAMD.route({
                        templateUrl: 'pages/members.html',
                        controller: 'MembersCtrl'
                    })).
                    when('/hot', angularAMD.route({
                        templateUrl: 'pages/hot.html',
                        controller: 'HotCtrl'
                    })).
                    when('/companies', angularAMD.route({
                        templateUrl: 'pages/companies.html',
                        controller: 'CompaniesCtrl'
                    })).
                    when('/profile/:id', angularAMD.route({
                        templateUrl: 'pages/profile.html',
                        controller: 'ProfileCtrl'
                    })).
                    when('/', angularAMD.route({
                        templateUrl: 'pages/home.html',
                        controller: 'HomeCtrl'
                    })).
                    otherwise({
                        redirectTo: '/'
                    });

                $locationProvider.html5Mode({
                    enabled: true,
                    requireBase: false
                });

                /*$httpProvider.interceptors.push(['$q', '$location', function($q, $location) {
                    return {
                        'responseError': function (rejection) {
                            var defer = $q.defer();

                            if(rejection.status == 401){
                                $location.path('/');
                            }

                            defer.reject(rejection);

                            return defer.promise;
                        }
                    };
                }]);*/
            }]);
    });