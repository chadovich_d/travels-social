/**
 * Created by chadovich_cr on 3/14/16.
 */

define(['app'], function (app) {
    app.factory('utils', [
        '$http', '$q', '$location',
        '$rootScope', '$timeout', '$cookies',
        '$routeParams',
        function($http, $q, $location, $rootScope,
                 $timeout, $cookies, $routeParams) {

            /**
             * is active page
             * uses to find out which of pages is active now
             * (navigation)
             * @returns function
             */
            function _isActivePage() {
                return function(path) {
                    return path === $location.path();
                };
            }

            /**
             * is main page
             * uses to find out whether active page is main page (index)
             * @returns function
             */
            function _isMainPage() {
                return function() {
                    return $location.path() === '/';
                };
            }

            /**
             * is logged in
             * getter for user's 'loggedIn' status
             * (after user log in
             * status 'loggedIn' changes to 'true'
             * and saves to localStorage or cookies)
             * @returns boolean
             */
            function _isLoggedIn() {
                var localStorageLoggedIn = localStorage.getItem('travels_social_loggedIn');
                var sessionLoggedIn = $cookies.get('travels_social_loggedIn');

                if(localStorageLoggedIn === 'true' || sessionLoggedIn === 'true') {
                    return true;
                } else {
                    return false;
                }
            }

            /**
             * load interests
             * loads interests list
             * ('create account' form on main page)
             * @param limit
             * @returns {*}
             */
            function loadInterests(limit) {
                return $q(function(resolve, reject) {
                    var req = {
                        method: 'GET',
                        url: '/api/v1/interests'
                    };

                    if(limit) {
                        req.params = {limit: limit};
                    }

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * get geolocation
             * gets user's current geolocation
             * ('create account' form on main page)
             * @returns {*}
             * @private
             */
            function _getGeolocation() {
                if ("geolocation" in navigator) {
                    return $q(function(resolve, reject) {
                        var geolocationData = {};

                        var success = function(position) {
                            geolocationData.latitude = position.coords.latitude;
                            geolocationData.longitude = position.coords.longitude;
                            resolve(geolocationData);
                        };

                        var error = function(err) {
                            console.warn('ERROR(' + err.code + '): ' + err.message);
                            reject(geolocationData);
                        };

                        var options = {
                            enableHighAccuracy: true,
                            timeout: Infinity,
                            maximumAge: 0
                        };

                        navigator.geolocation.getCurrentPosition(success, error, options);
                    });
                }
            }

            /**
             * decode geolocation
             * decodes and returns geaolocation data
             * @param lat
             * @param lng
             * @param area
             * @returns {*}
             * @private
             */
            function _decodeGeolocation(lat, lng, area) {
                return $q(function(resolve, reject) {

                    var geocoder = new google.maps.Geocoder;
                    var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};

                    geocoder.geocode({'location': latlng}, function(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {

                            /**
                             * results can be limited with type of address
                             * (country, postalCode, locality e.t.c.)
                             */
                            if(area) {
                                (function(type) {
                                    for(var i = 0, len = results.length; i < len; i++) {
                                        for(var j = 0, typeLen = results[i].types.length; j < typeLen; j++) {
                                            if(results[i].types[j] === type) {
                                                resolve(results[i].formatted_address);
                                            }
                                        }
                                    }
                                }(area));
                            } else {
                                resolve(results);
                            }

                        } else {
                            reject(status);
                        }
                    });
                });
            }

            /**
             * notifications are used to display server side warnings
             * and hints for users
             * @param type
             * @param message
             * @private
             */
            function _showNotification(type, message) {
                var notifications = $(document).find('#notifications');

                $rootScope.notificationStatus = message.header;
                $rootScope.notificationText = message.text;

                notifications.addClass(type);
                notifications.toggleClass('display-block');

                $timeout(function() {
                    _hideNotification(type);
                }, 5000);
            }

            /**
             * hide notification
             * @param type
             * @private
             */
            function _hideNotification(type) {
                var notifications = $(document).find('#notifications');

                notifications.removeClass(type);
                notifications.toggleClass('display-block');
                $rootScope.notificationStatus = '';
                $rootScope.notificationText = '';
            }

            /**
             * Create user API request
             * @param data
             * @returns Promise
             * @private
             */
            function _createUser(data) {
                return $q(function(resolve, reject) {
                    var req = {
                        method: 'POST',
                        url: '/api/v1/users',
                        data: JSON.stringify(data)
                    };

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * Log in user API request
             * @param data
             * @returns Promise
             * @private
             */
            function _loginUser(data) {
                return $q(function(resolve, reject) {
                    var req = {
                        method: 'POST',
                        url: '/api/v1/login',
                        data: JSON.stringify(data)
                    };

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * Log out API request
             * @param userId
             * @returns Promise
             * @private
             */
            function _logOutUser(userId) {
                return $q(function(resolve, reject) {
                    var req = {
                        method: 'POST',
                        url: '/api/v1/logout',
                        data: {id: userId}
                    };

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * Saving to localStorage user data after log in
             * in order to remember user
             * @param user
             * @private
             */
            function _saveToLocalStorage(user) {

                // Clearing login information before inserting new data
                _removeLoginData();

                localStorage.setItem('travels_social_user', user.id);
                localStorage.setItem('travels_social_token', user.token);
                localStorage.setItem('travels_social_loggedIn', user.loggedIn);
                localStorage.setItem('travels_social_firstName', user.firstName);
                localStorage.setItem('travels_social_lastName', user.lastName);
            }

            /**
             * Saving to cookies in order to remember user
             * only during session
             * @param user
             * @private
             */
            function _saveToCookies(user) {

                // Clearing login information before inserting new data
                _removeLoginData();

                $cookies.put('travels_social_user', user.id);
                $cookies.put('travels_social_token', user.token);
                $cookies.put('travels_social_loggedIn', user.loggedIn);
                $cookies.put('travels_social_firstName', user.firstName);
                $cookies.put('travels_social_lastName', user.lastName);
            }

            /**
             * Clearing log in data from all storages
             * @private
             */
            function _removeLoginData() {
                localStorage.removeItem('travels_social_user');
                localStorage.removeItem('travels_social_token');
                localStorage.removeItem('travels_social_loggedIn');
                localStorage.removeItem('travels_social_firstName');
                localStorage.removeItem('travels_social_lastName');

                $cookies.remove('travels_social_user');
                $cookies.remove('travels_social_token');
                $cookies.remove('travels_social_loggedIn');
                $cookies.remove('travels_social_firstName');
                $cookies.remove('travels_social_lastName');
            }

            /**
             * Update log in data in rootScope after user's
             * log in/log out
             * @private
             */
            function _updateLoginData() {
                $rootScope.loggedIn = _isLoggedIn();
                $rootScope.token = _getToken();
                $rootScope.userId = _getUserId();
                $rootScope.userFirstName = _getUserFirstName();
                $http.defaults.headers.common.Authorization = 'Bearer ' + _getToken();
            }

            /**
             * Get token from localStorage or cookies
             * @returns {*|string}
             * @private
             */
            function _getToken() {
                var localStorageToken = localStorage.getItem('travels_social_token');
                var sessionToken = $cookies.get('travels_social_token');

                return localStorageToken || sessionToken;
            }

            /**
             * Get user's id from localStorage or cookies
             * @returns {*|string}
             * @private
             */
            function _getUserId() {
                var localStorageUserId = localStorage.getItem('travels_social_user');
                var sessionUserId = $cookies.get('travels_social_user');

                return localStorageUserId || sessionUserId;
            }

            /**
             * Get user's first name from localStorage or cookies
             * @returns {*|string}
             * @private
             */
            function _getUserFirstName() {
                var localStorageUserFirstName = localStorage.getItem('travels_social_firstName');
                var sessionUserFirstName = $cookies.get('travels_social_firstName');

                return localStorageUserFirstName || sessionUserFirstName;
            }

            /**
             * Get user profile API request
             * @returns Promise
             * @private
             */
            function _getUserProfile() {
                return $q(function(resolve, reject) {
                    var id = $routeParams.id;
                    var token = _getToken();
                    var req = {
                        method: 'GET',
                        url: '/api/v1/profile/' + id,
                        headers: {token: token}
                    };

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * Get users list (members page) API request
             * @param limit
             * @param offset
             * @returns Promise
             */
            function getUserList(limit, offset) {
                return $q(function(resolve, reject) {
                    var req = {
                        method: 'GET',
                        url: '/api/v1/users'
                    };

                    if(limit || offset) {
                        req.params = {
                            limit: limit,
                            offset: offset
                        }
                    }

                    if($rootScope.userId) {
                        req.params.userId = $rootScope.userId;
                    }

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * Helper function for rating calculate
             * Returns two Arrays with 'earned' and 'blank' stars
             * in order to repeat them in ng-repeat
             * @param rating
             * @returns {{earnedStars: Array, blankStars: Array}}
             * @private
             */
            function _setRating(rating) {
                var max = 5; // Maximum rating - 5 stars
                var earnedStars = [];
                var blankStars = [];

                for(var i = 1; i <= max; i++) {
                    if(i <= rating) {
                        earnedStars.push(i);
                    } else {
                        blankStars.push(i);
                    }
                }

                return {
                    earnedStars: earnedStars,
                    blankStars: blankStars
                }
            }

            /**
             * Add like API request
             * @param toUserId
             * @returns Promise
             * @private
             */
            function _addLike(toUserId) {
                return $q(function(resolve, reject) {

                    var req = {
                        method: 'POST',
                        url: '/api/v1/likes',
                        data: {toUserId: toUserId}
                    };

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * Delete like API request
             * @param toUserId
             * @returns Promise
             * @private
             */
            function _deleteLike(toUserId) {
                return $q(function(resolve, reject) {

                    var req = {
                        method: 'DELETE',
                        url: '/api/v1/likes',
                        params: {toUserId: toUserId}
                    };

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * Add 'follow' API request
             * @param toUserId
             * @returns Promise
             * @private
             */
            function _addFollow(toUserId) {
                return $q(function(resolve, reject) {

                    var req = {
                        method: 'POST',
                        url: '/api/v1/follow',
                        data: {toUserId: toUserId}
                    };

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }

            /**
             * Delete 'follow' API request
             * @param toUserId
             * @returns Promise
             * @private
             */
            function _deleteFollow(toUserId) {
                return $q(function(resolve, reject) {

                    var req = {
                        method: 'DELETE',
                        url: '/api/v1/follow',
                        params: {toUserId: toUserId}
                    };

                    $http(req).then(function(result) {
                        resolve(result);
                    }, function(err) {
                        reject(err);
                    });
                });
            }


            return {
                loadInterests: loadInterests,

                notification : {
                    show: _showNotification,
                    hide: _hideNotification
                },

                geolocation: {
                    get: _getGeolocation,
                    decode: _decodeGeolocation
                },

                page: {
                    isMain: _isMainPage,
                    isActive: _isActivePage
                },

                user: {
                    create: _createUser,
                    login: _loginUser,
                    logout: _logOutUser,
                    isLoggedIn: _isLoggedIn,
                    getProfile: _getUserProfile,
                    setRating: _setRating
                },

                loginData: {
                    saveToLocalStorage: _saveToLocalStorage,
                    saveToCookies: _saveToCookies,
                    getToken: _getToken,
                    getUserId: _getUserId,
                    getFirstName: _getUserFirstName,
                    remove: _removeLoginData,
                    update: _updateLoginData
                },

                likes: {
                    add: _addLike,
                    remove: _deleteLike
                },

                follows: {
                    add: _addFollow,
                    remove: _deleteFollow
                },

                getUserList: getUserList
            };
        }]);
});