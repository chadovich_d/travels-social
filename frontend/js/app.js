/**
 * Created by chadovich_cr on 3/14/16.
 */
define([
    'angular',
    'angularAMD',
    'angular-route',
    'domReady!',
    'ngCookies',
    'ui.bootstrap'
],
    function (angular, angularAMD) {
        var app = angular.module('app', [
            'ngRoute',
            'ngCookies',
            'ui.bootstrap'
        ]);

        return angularAMD.bootstrap(app);
    });