/**
 * Created by chadovich_cr on 3/16/16.
 */
define(['app', 'jquery'], function (app) {
    app.controller('ArticleCtrl', ['$scope',
        function($scope) {

            // Temporary patch
            // Will be replaced as soon as server side logic is ready
            $scope.author = {
                avatar: '../img/blog-article-author.jpg',
                name: 'MatArt'
            };

        }]);
});