/**
 * Created by chadovich_cr on 3/14/16.
 */

define(['app', 'jquery'], function (app) {
    app.controller('BlogCtrl', ['$scope',
        function($scope) {

            $scope.blogItems = [];
            $scope.mostCommented = [];

            // Temporary patch
            // Will be replaced as soon as server side logic is ready
            for(var i = 0; i < 12; i++) {
                var item = {
                    img: '../img/blog-item-img_2.png',
                    title: 'How to Capture the Moment',
                    text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. ' +
                        'Aspernatur consequuntur debitis.',
                    moreInfo: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.' +
                        'Ab adipisci alias aliquid aperiam aut eveniet facilis libero magnam tempore vel.' +
                        'Lorem ipsum dolor sit amet, consectetur adipisicing elit.' +
                        'Ab adipisci alias vel.',
                    authorAvatar: '../img/blog-item-author.png',
                    author: 'MatArt',
                    views: 12222,
                    comments: 23

                };
                $scope.blogItems.push(item);
            }

            // Temporary patch
            // Will be replaced as soon as server side logic is ready
            for(var k = 0; k < 3; k++) {
                var item = {
                    counter: 380,
                    title: 'Switching off the conversation on 501 Places',
                    date: '30 June 2012'

                };
                $scope.mostCommented.push(item);
            }

            $scope.showAdditionalInfo = function(index) {
                var item = $('.blog-item-' + index);

                $('.additional-info').hide();
                item.find('.additional-info').show();
                $('.blog-item').css('opacity', '0.15');
                item
                    .css('opacity', '1')
                    .css('z-index', '2');
                $('.blog-modal-wrapper').show();
            };

            $scope.hideAdditionalInfo = function() {
                $('.additional-info').hide();
                $('.blog-item')
                    .css('opacity', '1')
                    .css('z-index', '0');
                $('.blog-modal-wrapper').hide();
            };

        }]);
});