/**
 * Created by chadovich_cr on 3/18/16.
 */
define(['app', 'jquery', 'utils'], function (app) {
    app.controller('ProfileCtrl', ['$scope', 'utils', '$location',
        function($scope, utils, $location) {

            $scope.user = {};

            var getProfile = utils.user.getProfile();

            /**
             * Loads user's data from DB
             */
            getProfile.then(function(result) {
                $scope.user = result.data.user;

                if($scope.user.userInterests) {
                    $scope.user.interests = $scope.user.userInterests.split(',');
                } else {
                    $scope.user.interests = ['No interests'];
                }

                _setInterests(result);
                _setRating($scope.user.postsRating);
                _initMap($scope.user.currentPlaceLat, $scope.user.currentPlaceLng);

                if($scope.user.currentPlace === null) {
                    $scope.user.currentPlace = 'On a trip...';
                }

            }, function(err) {
                console.log(err);
                utils.showNotification('error', {header: err.status, text: err.data.message || err.data.details});
                $location.path('/members');
            });

            function _setInterests(result) {
                var list = result.data.interests;

                angular.forEach(list, function(value, key) {
                    $scope.user.interests.push(value.name);
                });
            }

            function _setRating(rating) {
                var userRating = utils.user.setRating(rating);
                $scope.user.earnedStars = userRating.earnedStars;
                $scope.user.blankStars = userRating.blankStars;
            }

            function _initMap(lat, lng) {

                var map = new google.maps.Map(document.getElementById('member-profile-map'), {
                    zoom: 3,
                    scrollwheel: false
                });

                // Set user's current place marker only if
                // one of his current place coordinate !== null
                if(lat || lng) {
                    var marker = new google.maps.Marker({
                        position: {lat: lat, lng: lng},
                        map: map,
                        title: $scope.user.firstName + ' is here now!'
                    });
                }

                map.setCenter({lat: lat || 50.51, lng: lng || -32.43});

                map.addListener('click', function(e) {
                    var marker = new google.maps.Marker({
                        position: {lat: e.latLng.lat(), lng: e.latLng.lng()},
                        map: map
                    });
                });
            }

            /**
             * OnClick for likes button.
             * Checks whether the user already liked the profile.
             * If not - add like, if did - remove like.
             * @param user
             */
            $scope.likeAction = function(user) {
                if(!!user.liked) {
                    _deleteLike(user);
                } else {
                    _addLike(user);
                }
            };

            /**
             * Adds 'like', increments 'profileLikesCount'
             * @param user
             * @private
             */
            function _addLike(user) {
                utils.likes.add(user.id).then(function(result) {
                    user.profileLikesCount += 1;
                    user.liked = true;
                    $('.member-like i.fa.fa-heart').addClass('liked');
                }, function(err) {
                    console.log(err);
                    utils.showNotification('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            /**
             * Removes 'like', decrements 'profileLikesCount'
             * @param user
             * @private
             */
            function _deleteLike(user) {
                utils.likes.remove(user.id).then(function(result) {
                    user.profileLikesCount -= 1;
                    user.liked = false;
                    $('.member-like i.fa.fa-heart').removeClass('liked');
                }, function(err) {
                    console.log(err);
                    utils.showNotification('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            /**
             * Checks whether the user already liked the profile.
             * @param liked
             * @returns {boolean}
             */
            $scope.alreadyLiked = function(liked) {
                // returns boolean value of 'liked'
                return !!liked;
            };

            /**
             * OnClick for 'follow' button.
             * Checks whether the user already followed the profile.
             * If not - add 'follow', if did - remove 'follow'.
             * @param user
             */
            $scope.followAction = function(user) {
                if(!!user.followed) {
                    _deleteFollow(user);
                } else {
                    _addFollow(user);
                }
            };

            /**
             * Adds 'follow', increments 'profileFollowersCount'
             * @param user
             * @private
             */
            function _addFollow(user) {
                utils.follows.add(user.id).then(function(result) {
                    user.profileFollowersCount += 1;
                    user.followed = true;
                    $('.member-' + user.id + ' .follow > button').addClass('active');
                }, function(err) {
                    console.log(err);
                    utils.showNotification('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            /**
             * Removes 'follow', decrements 'profileFollowersCount'
             * @param user
             * @private
             */
            function _deleteFollow(user) {
                utils.follows.remove(user.id).then(function(result) {
                    user.profileFollowersCount -= 1;
                    user.followed = false;
                    $('.member-' + user.id + ' .follow > button').removeClass('active');
                }, function(err) {
                    console.log(err);
                    utils.showNotification('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            /**
             * Checks whether the user already followed the profile.
             * @param followed
             * @returns {boolean}
             */
            $scope.alreadyFollowed = function(followed) {
                // returns boolean value of 'followed'
                return !!followed;
            };

        }]);
});