/**
 * Created by chadovich_cr on 3/14/16.
 */

/*define(['app', 'jquery', 'utils'], function (app) {*/
define(['app', 'utils'], function (app) {
    app.controller('MembersCtrl', ['$scope', '$location', 'utils',
        function($scope, $location, utils) {

            $scope.members = [];
            $scope.paginationLimit = 3; // members per 1 pagination page
            $scope.paginationOffset = 0; // initial pagination offset
            $scope.currentPage = 1; // initial page for pagination

            /**
             * Load first portion of members
             * @type {Promise}
             */
            var loadMembersList = utils.getUserList($scope.paginationLimit, $scope.paginationOffset);

            loadMembersList.then(function(result) {

                _setPagination(result);
                _setMembersList(result);

            }, function(err) {
                console.log(err);
                utils.notification.show('error', {header: err.status, text: err.data.message || err.data.details});
            });

            /**
             * After loading portion of members, parse 'result' Array and
             * set each user:
             * - 'currentPlace' if not defined in DB;
             * - 'postsRating' via helper function 'setRating';
             * - 'userInterests' list
             * @param result
             * @private
             */
            function _setMembersList(result) {
                var list = result.data.users;

                angular.forEach(list, function(value, key) {
                    $scope.members.push(value);

                    if($scope.members[key].currentPlace === null) {
                        $scope.members[key].currentPlace = 'On a trip...';
                    }

                    var userRating = utils.user.setRating($scope.members[key].postsRating);
                    $scope.members[key].earnedStars = userRating.earnedStars;
                    $scope.members[key].blankStars = userRating.blankStars;

                    if($scope.members[key].userInterests) {
                        $scope.members[key].userInterestsList = $scope.members[key].userInterests.split(',');
                    } else {
                        $scope.members[key].userInterestsList = ['No interests'];
                    }
                });
            }

            /**
             * Get users amount (number) from DB and set it as max limit of
             * pagination pages
             * @param result
             * @private
             */
            function _setPagination(result) {
                $scope.usersAmount = result.data.usersAmount;
            }

            /**
             * OnChange function for pagination. Loads another portion of
             * members with changed 'offset' and 'limit' params
             */
            $scope.loadPaginationPortion = function() {
                $scope.paginationOffset = ($scope.currentPage - 1) * $scope.paginationLimit;

                var loadMembersList = utils.getUserList($scope.paginationLimit, $scope.paginationOffset);

                loadMembersList.then(function(result) {

                    $scope.members = [];
                    _setMembersList(result);

                }, function(err) {
                    console.log(err);
                    utils.notification.show('error', {header: err.status, text: err.data.message || err.data.details});
                });
            };

            /**
             * Shows additional pop up block about user
             * @param index
             * @param id
             * @param lat
             * @param lng
             */
            $scope.showMemberPopup = function(index, id, lat, lng) {
                var member = $('.member-' + id);
                var memberPopup = member.find('.member-popup');
                var backGround = member.css('background-color');
                var memberMapId = 'member-' + id + '-map';

                member
                    .css('z-index', 3)
                    .css('background-color', backGround);
                member.addClass('popup-target');
                memberPopup
                    .css('background-color', backGround)
                    .slideDown();
                $('.modal-wrapper').show();

                _initMap(index, memberMapId, lat, lng);
            };

            /**
             * OnClick function for 'Get in profile' button in
             * user's additional pop up block
             * @param id
             */
            $scope.relocateToProfile = function(id) {
                $('.modal-wrapper').css('display', 'none');
                $location.path('/profile/' + id);
            };

            $('.modal-wrapper').on('click', function(){
                var popupTarget = $('.popup-target');
                var memberPopup = popupTarget.find('.member-popup');

                popupTarget
                    .css('z-index', 0)
                    .removeClass('popup-target');
                memberPopup.hide();
                $(this).hide();
            });

            /**
             * Initialize Google map
             * ( and adds marker for 'currentPlace' data from DB )
             * @param index
             * @param memberMapId
             * @param lat
             * @param lng
             * @private
             */
            function _initMap(index, memberMapId, lat, lng) {
                var map = new google.maps.Map(document.getElementById(memberMapId), {
                    zoom: 3,
                    scrollwheel: false
                });

                // Set user's current place marker only if
                // one of his current place coordinate !== null
                if(lat || lng) {
                    var marker = new google.maps.Marker({
                        position: {lat: lat, lng: lng},
                        map: map,
                        title: $scope.members[index].firstName + ' is here now!'
                    });
                }

                map.setCenter({lat: lat || 50.51, lng: lng || -32.43});
            }

            /**
             * OnClick for likes button.
             * Checks whether the user already liked the profile.
             * If not - add like, if did - remove like.
             * @param user
             */
            $scope.likeAction = function(user) {
                if(!!user.liked) {
                    _deleteLike(user);
                } else {
                    _addLike(user);
                }
            };

            /**
             * Adds 'like', increments 'profileLikesCount'
             * @param user
             * @private
             */
            function _addLike(user) {
                utils.likes.add(user.id).then(function(result) {
                    user.profileLikesCount += 1;
                    user.liked = true;
                    $('.member-' + user.id + ' i.fa.fa-heart').addClass('liked');
                }, function(err) {
                    console.log(err);
                    utils.notification.show('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            /**
             * Removes 'like', decrements 'profileLikesCount'
             * @param user
             * @private
             */
            function _deleteLike(user) {
                utils.likes.remove(user.id).then(function(result) {
                    user.profileLikesCount -= 1;
                    user.liked = false;
                    $('.member-' + user.id + ' i.fa.fa-heart').removeClass('liked');
                }, function(err) {
                    console.log(err);
                    utils.notification.show('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            /**
             * Checks whether the user already liked the profile.
             * @param liked
             * @returns {boolean}
             */
            $scope.alreadyLiked = function(liked) {
                // returns boolean value of 'liked'
                return !!liked;
            };

            /**
             * OnClick for 'follow' button.
             * Checks whether the user already followed the profile.
             * If not - add 'follow', if did - remove 'follow'.
             * @param user
             */
            $scope.followAction = function(user) {
                if(!!user.followed) {
                    _deleteFollow(user);
                } else {
                    _addFollow(user);
                }
            };

            /**
             * Adds 'follow', increments 'profileFollowersCount'
             * @param user
             * @private
             */
            function _addFollow(user) {
                utils.follows.add(user.id).then(function(result) {
                    user.profileFollowersCount += 1;
                    user.followed = true;
                    $('.member-' + user.id + ' .follow > button').addClass('active');
                }, function(err) {
                    console.log(err);
                    utils.notification.show('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            /**
             * Removes 'follow', decrements 'profileFollowersCount'
             * @param user
             * @private
             */
            function _deleteFollow(user) {
                utils.follows.remove(user.id).then(function(result) {
                    user.profileFollowersCount -= 1;
                    user.followed = false;
                    $('.member-' + user.id + ' .follow > button').removeClass('active');
                }, function(err) {
                    console.log(err);
                    utils.notification.show('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            /**
             * Checks whether the user already followed the profile.
             * @param followed
             * @returns {boolean}
             */
            $scope.alreadyFollowed = function(followed) {
                // returns boolean value of 'followed'
                return !!followed;
            };

        }]);
});