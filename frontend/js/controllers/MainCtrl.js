/**
 * Created by chadovich_cr on 3/14/16.
 */

define(['app', 'domReady!', 'utils', 'jquery-validate'], function (app) {
    app.controller('MainCtrl', [
        '$scope',
        '$http',
        'utils',
        '$rootScope',
        '$timeout',
        '$location',
        '$cookies',

        function($scope, $http, utils, $rootScope, $timeout, $location, $cookies) {

            /**
             * Additional method for jQueryValidate lib
             * This method validates RegExp sentences
             */
            $.validator.addMethod("pattern", function(value, element, param) {
                if (this.optional(element)) {
                    return true;
                }
                if (typeof param === "string") {
                    param = new RegExp("^(?:" + param + ")$");
                }
                return param.test(value);
            }, "Invalid format.");

            /**
             * Set default http headers for authorization
             * @type {string}
             */
            $http.defaults.headers.common.Authorization = 'Bearer ' + utils.loginData.getToken();

            $scope.isActive = utils.page.isActive();
            $scope.isMainPage = utils.page.isMain();

            $scope.interests = [];
            $scope.userCurrentPosition = {};

            $scope.hideNotification = utils.notification.hide;

            /**
             * Update user's log in data every time
             * user enters the site
             */
            utils.loginData.update();

            /**
             * Loads list of interests for 'create account' form
             * on main page. Limit is set to 9 because this is the most
             * optimal number of sings.
             * @type {*}
             */
            var loadInterests = utils.loadInterests(9);
            loadInterests.then(function(result) {
                setInterests(result);
            }, function(reason) {
                console.log(reason);
            });

            function setInterests(result) {
                var list = result.data.interests;

                angular.forEach(list, function(value, key) {
                    $scope.interests.push(value);
                });
            }

            /**
             * Gets user's current geoposition. This data
             * will be saved into DB as 'userCurrentPosition'
             * @type {*|string|Object|HttpPromise|String}
             */
            var getGeolocationData = utils.geolocation.get();
            getGeolocationData.then(function(result) {
                $scope.userCurrentPosition = result;

                /**
                 * Decodes user's current position. This data will be saved
                 * into DB as 'userCurrentPlace'
                 * @type {*|String|Array}
                 */
                var decodeGeolocation = utils.geolocation.decode(result.latitude, result.longitude, 'administrative_area_level_1');
                decodeGeolocation.then(function(result) {
                    if(result) {
                        $scope.userCurrentPlace = result;
                    } else {
                        $scope.userCurrentPlace = 'On a trip...';
                    }
                }, function(err) {
                    console.log(err);
                });

            }, function(reason) {
                console.log(reason);
            });

            $scope.offcanvasSearchIsShown = false;
            $scope.offcanvasMenuIsShown = false;

            $scope.showOffCanvasMenu = function() {
                $scope.offcanvasMenuIsShown = true;
            };

            $scope.hideOffCanvasMenu = function() {
                $scope.offcanvasMenuIsShown = false;
            };

            $scope.displayOffCanvasSearch = function() {
                if($scope.offcanvasSearchIsShown) {
                    $('#offcanvas-menu').css('overflow', 'hidden');
                    $('.offcanvas-search').removeClass('visible');
                    $scope.offcanvasSearchIsShown = false;
                } else {
                    $('#offcanvas-menu').css('overflow', 'visible');
                    $('.offcanvas-search').addClass('visible');
                    $scope.offcanvasSearchIsShown = true;
                }
            };

            $('#offcanvas-menu li a[href*="/"], .page-wrapper').on('click', function() {
                if(  $scope.offcanvasSearchIsShown) {
                    $('#offcanvas-menu').css('overflow', 'hidden');
                    $('.offcanvas-search').removeClass('visible');
                    $scope.offcanvasSearchIsShown = false;
                }
            });

            $scope.showSecondAuthStep = function() {
                $('.first-step').css('height', '0');
            };

            $scope.showFirstAuthStep = function() {
                $('.first-step').css('height', '370px');
            };

            $scope.checkAuthorizationForm = function() {
                var authorizationForm = $('#authorization-form');

                authorizationForm.validate({
                    rules: {
                        userFirstName: {
                            required: true,
                            pattern: /^[A-zА-яЁё]*$/
                        },
                        userLastName: {
                            required: true,
                            pattern: /^[A-zА-яЁё]*$/
                        },
                        userEmail: {
                            required: true,
                            email: true
                        },
                        userCountry: {
                            required: true,
                            pattern: /^[A-zА-яЁё\s-]*$/,
                            minlength: 2
                        },
                        userCity: {
                            required: true,
                            pattern: /^[A-zА-яЁё\s-]*$/,
                            minlength: 2
                        },
                        userPassword: {
                            required: true,
                            minlength: 6
                        },
                        userPasswordConfirm: {
                            required: true,
                            equalTo: '#user-password-create'
                        }
                    },
                    messages: {
                        userFirstName: {
                            required: 'Please, enter your name',
                            pattern: 'Only letters are allowed!'
                        },
                        userLastName: {
                            required: 'Please, enter your last name',
                            pattern: 'Only letters are allowed!'
                        },
                        userEmail: {
                            required: 'Please, enter your e-mail'
                        },
                        userCountry: {
                            required: 'Please, enter your country',
                            pattern: 'Only letters are allowed!'
                        },
                        userCity: {
                            required: 'Please, enter your city',
                            pattern: 'Only letters are allowed!'
                        },
                        userPassword: {
                            required: 'Please, enter your password',
                            minlength: 'Password must contain at least 6 chars'
                        },
                        userPasswordConfirm: {
                            required: 'Please, confirm your password',
                            equalTo: 'Please, enter your password again'
                        }
                    },
                    errorPlacement: function(error, element) {
                        error.insertBefore(element);
                    },
                    errorElement: "span",
                    errorClass: "validateError",
                    submitHandler: function(form) {
                        $scope.showSecondAuthStep();
                    }
                });
            };

            function _createUser() {
                var userFirstName = $('#user-first-name').val();
                var userLastName = $('#user-last-name').val();
                var userEmail = $('#user-email').val();
                var userCountry = $('#user-country').val();
                var userCity = $('#user-city').val();
                var userPassword = $('#user-password-create').val();
                var userAbout = $('#user-about').val();

                var interestsList = $('#user-interests input[type="checkbox"]');
                var userInterests = [];
                for(var i = 0, length = interestsList.length; i < length; i++) {
                    if(interestsList[i].checked) {
                        userInterests.push(parseInt(interestsList[i].id));
                    }
                }

                var data = {
                    firstName: userFirstName,
                    lastName: userLastName,
                    email: userEmail,
                    password: userPassword,
                    fromCountry: userCountry,
                    fromCity: userCity,
                    currentPlaceLng: $scope.userCurrentPosition.longitude,
                    currentPlaceLat: $scope.userCurrentPosition.latitude,
                    currentPlace: $scope.userCurrentPlace,
                    aboutText: userAbout,
                    userInterests: userInterests
                };

                var createUser = utils.user.create(data);

                createUser.then(function(result) {

                    $('.modal-wrapper').hide();
                    $('.login-pop-up, .authorization-pop-up').hide();
                    utils.notification.show('success', {header: 'Success!', text: 'Now you can log in!'});

                }, function(err) {
                    utils.notification.show('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            $scope.authorization = function() {
                _createUser();
            };

            function _loginUser() {
                var email = $('#user-login').val();
                var password = $('#user-password').val();
                var rememberUser = $('#remember-user').prop('checked');

                var data = {
                    email: email,
                    password: password
                };

                var login = utils.user.login(data);

                login.then(function(result) {

                    var user = {
                        id: result.data.user,
                        firstName: result.data.firstName,
                        lastName: result.data.lastName,
                        token: result.data.token,
                        loggedIn: true
                    };

                    if(rememberUser) {
                        utils.loginData.saveToLocalStorage(user);
                    } else {
                        utils.loginData.saveToCookies(user);
                    }

                    utils.loginData.update();

                    $('.modal-wrapper').css('display', 'none');
                    $('.login-pop-up, .authorization-pop-up').css('display', 'none');
                    utils.notification.show('success', {header: 'Success', text: 'Now you are logged in!'});

                    $location.path('/profile/' + result.data.user);

                }, function(err) {
                    console.log(err);
                    utils.notification.show('error', {header: err.status, text: err.data.message || err.data.details});
                });
            }

            function _checkLoginForm() {
                var loginForm = $('#login-form');

                loginForm.validate({
                    rules: {
                        userLoginEmail: {
                            required: true,
                            email: true
                        },
                        userLoginPassword: {
                            required: true,
                            minlength: 6
                        }
                    },
                    messages: {
                        userLoginEmail: {
                            required: 'Please, enter your e-mail'
                        },
                        userLoginPassword: {
                            required: 'Please, enter your password',
                            minlength: 'At least 6 chars required'
                        }
                    },
                    errorPlacement: function(error, element) {
                        error.insertBefore(element);
                    },
                    errorElement: "span",
                    errorClass: "validateError",
                    submitHandler: function() {
                        _loginUser();
                    }
                });
            }

            $scope.login = function() {
                _checkLoginForm();
            };

            $scope.logOut = function(userId) {
                _logOut(userId);
            };

            function _logOut(userId) {
                var logOut = utils.user.logout(userId);
                logOut.then(function(result) {

                    utils.loginData.remove();
                    utils.loginData.update();
                    $location.path('/');

                }, function(err) {
                    console.log(err);
                });
            }

            $scope.scrollUp = function() {
                $('html, body').animate({ scrollTop: 0 }, 800);
                return false;
            };
        }]);
});