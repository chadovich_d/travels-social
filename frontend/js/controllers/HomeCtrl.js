/**
 * Created by chadovich_cr on 3/14/16.
 */

define(['app', 'domReady!', 'jquery', 'utils'], function (app) {
    app.controller('HomeCtrl', ['$scope', '$location', '$rootScope', 'utils',
        function($scope, $location, $rootScope, utils) {

            var map;
            function initMap() {
                map = new google.maps.Map(document.getElementById('map'), {
                    center: {lat: 50.51, lng: -32.43},
                    zoom: 4,
                    scrollwheel: false
                });

                map.addListener('click', function(e) {
                    var marker = new google.maps.Marker({
                        position: {lat: e.latLng.lat(), lng: e.latLng.lng()},
                        map: map
                    });
                });
            }

            $(document).ready(function() {
                initMap();

                $('#login, #create-account').on('click', function() {
                    $('.modal-wrapper').show();
                    $(this).css('z-index', 2).addClass('modal-btn');
                    $(this).siblings('div[class*="pop-up"]').show();
                });

                $('.modal-wrapper').on('click', function() {
                    $('.modal-btn').css('z-index', 0).removeClass('modal-btn');
                    $(this).hide();
                    $('.login-pop-up, .authorization-pop-up').hide();
                });
            });

        }]);
});