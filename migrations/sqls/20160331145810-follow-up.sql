CREATE TABLE IF NOT EXISTS `follows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fromUserId` int(10) unsigned NOT NULL,
  `toUserId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY uniqueFollowers (fromUserId, toUserId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TRIGGER `followAddTrigger` AFTER INSERT ON `follows`
FOR EACH ROW UPDATE users SET profileFollowersCount = profileFollowersCount + 1 WHERE users.id = NEW.toUserId;

CREATE TRIGGER `followDeleteTrigger` AFTER DELETE ON `follows`
FOR EACH ROW UPDATE users SET profileFollowersCount = profileFollowersCount - 1 WHERE users.id = OLD.toUserId;