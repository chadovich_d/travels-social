CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fromUserId` int(10) unsigned NOT NULL,
  `toUserId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY uniqueLikes (fromUserId, toUserId)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TRIGGER `likesAddTrigger` AFTER INSERT ON `likes`
FOR EACH ROW UPDATE users SET profileLikesCount = profileLikesCount + 1 WHERE users.id = NEW.toUserId;

CREATE TRIGGER `likesDeleteTrigger` AFTER DELETE ON `likes`
FOR EACH ROW UPDATE users SET profileLikesCount = profileLikesCount - 1 WHERE users.id = OLD.toUserId;