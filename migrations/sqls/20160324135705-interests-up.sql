CREATE TABLE IF NOT EXISTS `interests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `interests`
--

INSERT INTO `interests` (`id`, `name`) VALUES
(1, 'Travel'),
(2, 'Girls'),
(3, 'Bycicle'),
(4, 'Developers'),
(5, 'Design'),
(6, 'Node.js'),
(7, 'Front End Development'),
(8, 'Back End Development'),
(9, 'Full Stack Development'),
(10, 'Angular.js'),
(11, 'Photo'),
(12, 'Music');